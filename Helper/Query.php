<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ResourceConnection;

class Query extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->resourceConnection = $resourceConnection;
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }


    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }


    public function updateOrderLabel($order_id,  $label_url, $status){
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.shipment_label = ?,
                    t.shipment_label_status = ? 
                    WHERE t.entity_id = ?;";
        $connection->query($sql, [$label_url, $status, $order_id]);

        $sql = "UPDATE " . $this->_getTableName('sales_order_grid') . " t
                SET t.shipment_label_status = ?
                WHERE entity_id = ?;";
        $connection->query($sql, [ $status, $order_id]);

//        $sql = "UPDATE  " . $this->_getTableName('sales_order_grid') . " t
//                SET t.shipment_label_status = null
//                WHERE t.entity_id in ( SELECT so.entity_id FROM " . $this->_getTableName('sales_order') . " so WHERE so.status in ('complete','closed') AND so.shipment_label_status = 'ERR' AND
//                      so.shipment_label IS NOT NULL);";
//        $connection->query($sql, [ $status, $order_id]);
    }
}