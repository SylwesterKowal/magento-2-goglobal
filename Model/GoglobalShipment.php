<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model;

use Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface;
use Magento\Framework\Model\AbstractModel;

class GoglobalShipment extends AbstractModel implements GoglobalShipmentInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\GoGlobal\Model\ResourceModel\GoglobalShipment::class);
    }

    /**
     * @inheritDoc
     */
    public function getGoglobalshipmentId()
    {
        return $this->getData(self::GOGLOBALSHIPMENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setGoglobalshipmentId($goglobalshipmentId)
    {
        return $this->setData(self::GOGLOBALSHIPMENT_ID, $goglobalshipmentId);
    }

    /**
     * @inheritDoc
     */
    public function getReferenceId()
    {
        return $this->getData(self::REFERENCE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setReferenceId($referenceId)
    {
        return $this->setData(self::REFERENCE_ID, $referenceId);
    }


    /**
     * @inheritDoc
     */
    public function getCreated()
    {
        return $this->getData(self::CREATED);
    }

    /**
     * @inheritDoc
     */
    public function setCreated($created)
    {
        return $this->setData(self::CREATED, $created);
    }

    /**
     * @inheritDoc
     */
    public function getMessageUid()
    {
        return $this->getData(self::MESSAGE_UID);
    }

    /**
     * @inheritDoc
     */
    public function setMessageUid($messageUid)
    {
        return $this->setData(self::MESSAGE_UID, $messageUid);
    }

    /**
     * @inheritDoc
     */
    public function getMessageCode()
    {
        return $this->getData(self::MESSAGE_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setMessageCode($messageCode)
    {
        return $this->setData(self::MESSAGE_CODE, $messageCode);
    }

    /**
     * @inheritDoc
     */
    public function getMessageContent()
    {
        return $this->getData(self::MESSAGE_CONTENT);
    }

    /**
     * @inheritDoc
     */
    public function setMessageContent($messageContent)
    {
        return $this->setData(self::MESSAGE_CONTENT, $messageContent);
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getBarcode()
    {
        return $this->getData(self::BARCODE);
    }

    /**
     * @inheritDoc
     */
    public function setBarcode($barcode)
    {
        return $this->setData(self::BARCODE, $barcode);
    }

    /**
     * @inheritDoc
     */
    public function getTrackingNumber()
    {
        return $this->getData(self::TRACKING_NUMBER);
    }

    /**
     * @inheritDoc
     */
    public function setTrackingNumber($trackingNumber)
    {
        return $this->setData(self::TRACKING_NUMBER, $trackingNumber);
    }

    /**
     * @inheritDoc
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * @inheritDoc
     */
    public function getTrackingUrl()
    {
        return $this->getData(self::TRACKING_URL);
    }

    /**
     * @inheritDoc
     */
    public function setTrackingUrl($trackingUrl)
    {
        return $this->setData(self::TRACKING_URL, $trackingUrl);
    }

    /**
     * @inheritDoc
     */
    public function getLabelData()
    {
        return $this->getData(self::LABELDATA);
    }

    /**
     * @inheritDoc
     */
    public function setLabelData($labelData)
    {
        return $this->setData(self::LABELDATA, $labelData);
    }

    /**
     * @inheritDoc
     */
    public function getLabelFormat()
    {
        return $this->getData(self::LABELFORMAT);
    }

    /**
     * @inheritDoc
     */
    public function setLabelFormat($labelFormat)
    {
        return $this->setData(self::LABELFORMAT, $labelFormat);
    }

    /**
     * @inheritDoc
     */
    public function getKurier()
    {
        return $this->getData(self::KURIER);
    }

    /**
     * @inheritDoc
     */
    public function setKurier($kurier)
    {
        return $this->setData(self::KURIER, $kurier);
    }

    /**
     * @inheritDoc
     */
    public function getKurierService()
    {
        return $this->getData(self::KURIER_SERVICE);
    }

    /**
     * @inheritDoc
     */
    public function setKurierService($kurierService)
    {
        return $this->setData(self::KURIER_SERVICE, $kurierService);
    }

    /**
     * @inheritDoc
     */
    public function getKurierType()
    {
        return $this->getData(self::KURIER_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setKurierType($kurierType)
    {
        return $this->setData(self::KURIER_TYPE, $kurierType);
    }
}

