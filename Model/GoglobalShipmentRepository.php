<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model;

use Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface;
use Kowal\GoGlobal\Api\Data\GoglobalShipmentInterfaceFactory;
use Kowal\GoGlobal\Api\Data\GoglobalShipmentSearchResultsInterfaceFactory;
use Kowal\GoGlobal\Api\GoglobalShipmentRepositoryInterface;
use Kowal\GoGlobal\Model\ResourceModel\GoglobalShipment as ResourceGoglobalShipment;
use Kowal\GoGlobal\Model\ResourceModel\GoglobalShipment\CollectionFactory as GoglobalShipmentCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class GoglobalShipmentRepository implements GoglobalShipmentRepositoryInterface
{

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var GoglobalShipment
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceGoglobalShipment
     */
    protected $resource;

    /**
     * @var GoglobalShipmentInterfaceFactory
     */
    protected $goglobalShipmentFactory;

    /**
     * @var GoglobalShipmentCollectionFactory
     */
    protected $goglobalShipmentCollectionFactory;


    /**
     * @param ResourceGoglobalShipment $resource
     * @param GoglobalShipmentInterfaceFactory $goglobalShipmentFactory
     * @param GoglobalShipmentCollectionFactory $goglobalShipmentCollectionFactory
     * @param GoglobalShipmentSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceGoglobalShipment $resource,
        GoglobalShipmentInterfaceFactory $goglobalShipmentFactory,
        GoglobalShipmentCollectionFactory $goglobalShipmentCollectionFactory,
        GoglobalShipmentSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->goglobalShipmentFactory = $goglobalShipmentFactory;
        $this->goglobalShipmentCollectionFactory = $goglobalShipmentCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(
        GoglobalShipmentInterface $goglobalShipment
    ) {
        try {
            $this->resource->save($goglobalShipment);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the goglobalShipment: %1',
                $exception->getMessage()
            ));
        }
        return $goglobalShipment;
    }

    /**
     * @inheritDoc
     */
    public function get($goglobalShipmentId)
    {
        $goglobalShipment = $this->goglobalShipmentFactory->create();
        $this->resource->load($goglobalShipment, $goglobalShipmentId);
        if (!$goglobalShipment->getId()) {
            throw new NoSuchEntityException(__('GoglobalShipment with id "%1" does not exist.', $goglobalShipmentId));
        }
        return $goglobalShipment;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->goglobalShipmentCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(
        GoglobalShipmentInterface $goglobalShipment
    ) {
        try {
            $goglobalShipmentModel = $this->goglobalShipmentFactory->create();
            $this->resource->load($goglobalShipmentModel, $goglobalShipment->getGoglobalshipmentId());
            $this->resource->delete($goglobalShipmentModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the GoglobalShipment: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($goglobalShipmentId)
    {
        return $this->delete($this->get($goglobalShipmentId));
    }
}

