<?php

namespace Kowal\GoGlobal\Model\Service;

interface ErrorHandlerInterface
{
    public function handle($jsonResponse) : string;
}
