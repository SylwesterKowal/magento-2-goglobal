<?php

namespace Kowal\GoGlobal\Model\Service\Shipment;

use Psr\Log\LoggerInterface as PsrLoggerInterface;
use Kowal\GoGlobal\Model\ResourceModel\Goglobal\CollectionFactory as CollectionFactoryGoglobal;

class Create
{
    /**
     * @var
     */
    public $order;
    public $requestBody;
    public $requestBodyLabel;
    public $deliverySettings;
    public $country;

    /**
     * @param PsrLoggerInterface $logger
     */
    public function __construct(
        PsrLoggerInterface        $logger,
        CollectionFactoryGoglobal $collectionFactoryGoglobal
    )
    {
        $this->logger = $logger;
        $this->collectionFactoryGoglobal = $collectionFactoryGoglobal;
    }

    public function createBody($data, $order)
    {
        $this->order = $order;
        $this->shippingAddress = $this->order->getShippingAddress();
        $this->deliverySettings = $this->getGoglobalDeliverySettings();

        switch ($data['mode']) {
            case 'pro':
                $this->requestBody = [
                    "hash" => $data['hash'],
                    "CreateShipmentsRequest002" => [
                        "shipmentType" => "Delivery",
                        "carrierName" => $this->deliverySettings->getKurier(),
                        "shipperAddress" => [
                            "additionalDescription" => "description",
                        ],
                        "receiverAddress" => [
                            "nameOrCompany" => (($this->shippingAddress->getCompany()) ? ($this->shippingAddress->getCompany() . " ") : '') . $this->shippingAddress->getFirstname() . ' ' . $this->shippingAddress->getLastname(),
                            "additionalDescription" => "",
                            "address1" => implode(', ', $this->shippingAddress->getStreet()),
                            "address2" => "",
                            "city" => $this->shippingAddress->getCity(),
                            "zipCode" => $this->shippingAddress->getPostcode(),
                            "region" => "",
                            "country" => $this->country,
                            "email" => $this->order->getCustomerEmail(),
                            "phone" => $this->shippingAddress->getTelephone(),
                            "taxId" => $this->shippingAddress->getVatId(),
                            "pickupId" => null
                        ],
                        "payerAddress" => null,

                        "serviceLevel" => [
                            "service" => $this->deliverySettings->getSerwis(),
                            "cod" => null,
                            "insurance" => null
                        ],
                        "shipmentDescription1" => "description",
                        "shipmentDescription2" => ""
                    ]
                ];
                break;
            case 'dev':
                $this->setSampleData($data);
                break;
        }



        if ($this->country == 'GB') {
            $this->requestBody['CreateShipmentsRequest002']['items'] = [
                [
                    "referenceID" => $this->order->getIncrementId(),
                    "referenceID2" => "",
                    "type" => $this->deliverySettings->getTyp(),
                    "weight" => $this->deliverySettings->getWaga()
                ]
            ];

            $this->requestBody['CreateShipmentsRequest002']['invoiceNumber'] = $this->order->getIncrementId();
            $this->requestBody['CreateShipmentsRequest002']['orderValue'] = ($this->order->getGrandTotal() == 0) ? 1 : round($this->order->getGrandTotal(),2);
            $this->requestBody['CreateShipmentsRequest002']['currency'] = $this->order->getOrderCurrencyCode();
            $this->addProducts($data);
        }else{
            $this->requestBody['CreateShipmentsRequest002']['items'] = [
                    "referenceID" => $this->order->getIncrementId(),
                    "referenceID2" => "",
                    "type" => $this->deliverySettings->getTyp(),
                    "weight" => $this->deliverySettings->getWaga()
            ];
        }
    }


    public function createBodyLabel($data, $referenceID)
    {
        $this->requestBodyLabel = [
            "hash" => $data['hash'],
            "GetLabelRequest" => [
                "referenceID" => $referenceID,
                "collectiveLabel" => 1,
                "labelFormat" => $data['format_label'],
                "labelSize" => $data['file_type']
            ]
        ];
    }


    public function setSampleData($data)
    {
        $this->requestBody = [
            "hash" => $data['hash'],
            "CreateShipmentsRequest002" => [
                "shipmentType" => "Delivery",
                "carrierName" => "FR-COLISSIMO",
                "shipperAddress" => [
                    "additionalDescription" => "description",
                ],
                "receiverAddress" => [
                    "nameOrCompany" => "Lucas About",
                    "additionalDescription" => "",
                    "address1" => "17 Rue Baron le Roy",
                    "address2" => "",
                    "city" => "Paris",
                    "zipCode" => "75012",
                    "region" => "",
                    "country" => "FR",
                    "email" => "email@email.com",
                    "phone" => "123456765",
                    "taxId" => "",
                    "pickupId" => null
                ],
                "payerAddress" => null,
                "items" => [
                    [
                        "referenceID" => "GoLabel000013",
                        "referenceID2" => "",
                        "type" => "CS",
                        "weight" => "1.00"
                    ]
                ],
                "serviceLevel" => [
                    "service" => "STC",
                    "cod" => null,
                    "insurance" => null
                ],
                "shipmentDescription1" => "description",
                "shipmentDescription2" => ""
            ]
        ];

        return $this;
    }


    public function getGoglobalDeliverySettings()
    {

        $this->country = $this->shippingAddress->getCountryId();


        $collectionGoglobal = $this->collectionFactoryGoglobal->create();
        $collectionGoglobal->addFieldToFilter('kraj', array('eq' => $this->country));
        $collectionGoglobal->addFieldToSelect('*');
        $settings = $collectionGoglobal->getFirstItem();
        if (count($settings->getData())) {
            $this->logger->info(print_r($settings->getData(), true));
            return $settings;
        } else {
            $this->logger->info(print_r($this->country, true));
            $collectionGoglobal = $this->collectionFactoryGoglobal->create();
            $collectionGoglobal->addFieldToFilter('kraj', ['null' => true]);
            $collectionGoglobal->addFieldToSelect('*');
            $settings = $collectionGoglobal->getFirstItem();
            $this->logger->info(print_r($settings->getData(), true));
            return $settings;
        }
    }

    public function addProducts($data)
    {
        $items = $this->order->getAllItems();
        foreach ($items as $item) {
            $znizka = $item->getDiscountAmount();
            $price = $item->getPriceInclTax() - $znizka;

            $this->requestBody['CreateShipmentsRequest002']['items'][0]['products'][] = [
                'name' => $item->getName(),
                'number' => $item->getSku(),
                'quantity' => (int)$item->getQtyOrdered(),
                'price' => ($this->order->getGrandTotal() == 0) ? 1 : $price,
                'tariffCode' => $data['tariffCode'],
                'originCountry' => 'PL'
            ];
            break;
        }
    }

}
