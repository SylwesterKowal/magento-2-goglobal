<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model;

use Kowal\GoGlobal\Api\Data\GoglobalInterface;
use Magento\Framework\Model\AbstractModel;

class Goglobal extends AbstractModel implements GoglobalInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\GoGlobal\Model\ResourceModel\Goglobal::class);
    }

    /**
     * @inheritDoc
     */
    public function getGoglobalId()
    {
        return $this->getData(self::GOGLOBAL_ID);
    }

    /**
     * @inheritDoc
     */
    public function setGoglobalId($goglobalId)
    {
        return $this->setData(self::GOGLOBAL_ID, $goglobalId);
    }

    /**
     * @inheritDoc
     */
    public function getKraj()
    {
        return $this->getData(self::KRAJ);
    }

    /**
     * @inheritDoc
     */
    public function setKraj($kraj)
    {
        return $this->setData(self::KRAJ, $kraj);
    }

    /**
     * @inheritDoc
     */
    public function getKurier()
    {
        return $this->getData(self::KURIER);
    }

    /**
     * @inheritDoc
     */
    public function setKurier($kurier)
    {
        return $this->setData(self::KURIER, $kurier);
    }

    /**
     * @inheritDoc
     */
    public function getSerwis()
    {
        return $this->getData(self::SERWIS);
    }

    /**
     * @inheritDoc
     */
    public function setSerwis($serwis)
    {
        return $this->setData(self::SERWIS, $serwis);
    }

    /**
     * @inheritDoc
     */
    public function getTyp()
    {
        return $this->getData(self::TYP);
    }

    /**
     * @inheritDoc
     */
    public function setTyp($typ)
    {
        return $this->setData(self::TYP, $typ);
    }

    /**
     * @inheritDoc
     */
    public function getWaga()
    {
        return $this->getData(self::WAGA);
    }

    /**
     * @inheritDoc
     */
    public function setWaga($waga)
    {
        return $this->setData(self::WAGA, $waga);
    }

    /**
     * @inheritDoc
     */
    public function getMetoda()
    {
        return $this->getData(self::METODA);
    }

    /**
     * @inheritDoc
     */
    public function setMetoda($metoda)
    {
        return $this->setData(self::METODA, $metoda);
    }

    /**
     * @inheritDoc
     */
    public function getTrackingUrlTemplate()
    {
        return $this->getData(self::TRACKING_URL_TEMPLATE);
    }

    /**
     * @inheritDoc
     */
    public function setTrackingUrlTemplate($trackingUrlTemplate)
    {
        return $this->setData(self::TRACKING_URL_TEMPLATE, $trackingUrlTemplate);
    }
}

