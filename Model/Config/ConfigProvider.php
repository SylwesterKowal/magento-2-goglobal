<?php

namespace Kowal\GoGlobal\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\RepositoryFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\ConfigProviderInterface;
use Kowal\GoGlobal\Model\Config\Source\MagentoShippings;

class ConfigProvider
{


    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var ShippingMethods
     */
    private $shippingMethods;

    /**
     * @var RepositoryFactory
     */
    private $repositoryFactory;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param EncryptorInterface $encryptor
     * @param RepositoryFactory $repositoryFactory
     * @param MagentoShippings $magentoShippings
     */
    public function __construct(
        ScopeConfigInterface  $scopeConfig,
        StoreManagerInterface $storeManager,
        EncryptorInterface    $encryptor,
        RepositoryFactory     $repositoryFactory,
        MagentoShippings      $magentoShippings
    )
    {
        $this->encryptor = $encryptor;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->repositoryFactory = $repositoryFactory;
        $this->shippingMethods = $magentoShippings;
    }

    /**
     * @param $field
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getShippingConfigData($group, $field)
    {
        $path = 'goglobal/' . $group . '/' . $field;

        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    /**
     * @return mixed
     */
    public function getEnable()
    {
        return $this->getShippingConfigData('options', 'enable');
    }

    /**
     * @return mixed
     */
    public function getMode()
    {
        return $this->getShippingConfigData('api', 'mode');
    }

    /**
     * @return mixed
     */
    public function getApiBaseUrl()
    {
        $mode = $this->getMode();
        switch ($mode) {
            case 'pro':
                $field = 'api_url_pro';
                break;
            case 'dev':
                $field = 'api_url_dev';
                break;
            default:
                $field = 'api_url_dev';
        }
        return $this->getShippingConfigData('api', $field);
    }
    /**
     * @return mixed
     */
    public function getApiBaseUrlTracking()
    {
        $mode = $this->getMode();
        switch ($mode) {
            case 'pro':
                $field = 'api_url_tracking_pro';
                break;
            case 'dev':
                $field = 'api_url_tracking_dev';
                break;
            default:
                $field = 'api_url_tracking_dev';
        }
        return $this->getShippingConfigData('api', $field);
    }

    public function getFileType()
    {
        return $this->getShippingConfigData('api', 'api_typ_pliku');
    }

    public function getFormatLabel()
    {
        return $this->getShippingConfigData('api', 'api_format');
    }

    public function getWielkoscPpaczki()
    {
        return $this->getShippingConfigData('api', 'api_wielkosc_paczki');
    }

    public function getTariffCode()
    {
        return $this->getShippingConfigData('api', 'tariff_code');
    }

    public function getApiUsername()
    {
        return $this->getShippingConfigData('autoryzacja', 'api_username');
    }

    public function getApiPassword()
    {
        return $this->getShippingConfigData('autoryzacja', 'api_password');
    }

    public function getApiHash()
    {
        return $this->getShippingConfigData('autoryzacja', 'api_hash');
    }
}
