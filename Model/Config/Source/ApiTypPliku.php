<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model\Config\Source;

class ApiTypPliku implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'PDF', 'label' => __('PDF')],['value' => 'IMG', 'label' => __('IMG')]];
    }

    public function toArray()
    {
        return ['PDF' => __('PDF'),'IMG' => __('IMG')];
    }
}

