<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model\Config\Source;

class Mode implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'pro', 'label' => __('production')], ['value' => 'dev', 'label' => __('development')]];
    }

    public function toArray()
    {
        return ['pro' => __('production'), 'dev' => __('development')];
    }
}

