<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model\Config\Source;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Shipping\Model\Config;
use Magento\Store\Model\ScopeInterface;

class MagentoShippings implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Shipping\Model\Config $shippingConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $shippingConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->shippingConfig = $shippingConfig;
    }

    public function toOptionArray()
    {
        $methods = [['value' => '', 'label' => '']];
        $carriers = $this->shippingConfig->getAllCarriers();
        foreach ($carriers as $carrierCode => $carrierModel) {
            if (!$carrierModel->isActive()) {
                continue;
            }

            $carrierMethods = $carrierModel->getAllowedMethods();

            if (!$carrierMethods) {
                continue;
            }
            $carrierTitle = $this->scopeConfig->getValue(
                'carriers/' . $carrierCode . '/title',
                ScopeInterface::SCOPE_STORE
            );
            foreach ($carrierMethods as $methodCode => $methodTitle) {
                /** Check it $carrierMethods array was well formed */
                if (!$methodCode) {
                    continue;
                }

                $label = '[' . $carrierTitle . '] ' . $methodTitle;
                $methods[] = [
                    'value' => $carrierCode . '_' . $methodCode,
                    'label' => $label,
                ];
            }
        }
        return $methods;
    }

    public function toArray()
    {
        $aMethos = [];
        $methods = $this->toOptionArray();
        foreach ($methods as $method){
            $aMethos[$method['value']] = $method['label'];
        }
        return $aMethos;// ['Shipp1' => __('Shipp1'),'Shipp2' => __('Shipp2')];
    }
}

