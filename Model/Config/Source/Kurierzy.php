<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model\Config\Source;

class Kurierzy implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => 'DE-DPD', 'label' => __('DE-DPD')],
            ['value' => 'DE-DHL', 'label' => __('DE-DHL')],
            ['value' => 'GB-ROYALMAIL', 'label' => __('GB-ROYALMAIL')],
            ['value' => 'FR-COLISSIMO', 'label' => __('FR-COLISSIMO')],
            ['value' => 'PL-FEDEX', 'label' => __('PL-FEDEX')],
            ['value' => 'Parcel Post', 'label' => __('Parcel Post')],
            ['value' => 'GLS-IT', 'label' => __('GLS-IT')],
            ['value' => 'POST-AT', 'label' => __('POST-AT')],
            ['value' => 'GB-HERMES', 'label' => __('GB-HERMES')],
            ['value' => 'ES-CORREOS', 'label' => __('ES-CORREOS')],
            ['value' => 'POST-NL', 'label' => __('POST-NL')],
            ['value' => 'UA-NOVA-POSHTA', 'label' => __('PUA-NOVA-POSHTA')],
        ];
    }

    public function toArray()
    {
        $aKuriers = [];
        $kuriers = $this->toOptionArray();
        foreach ($kuriers as $kurier){
            $aKuriers[$kurier['value']] = $kurier['label'];
        }
        return $aKuriers;
    }
}

