<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model;

use Kowal\GoGlobal\Api\Data\GoglobalInterface;
use Kowal\GoGlobal\Api\Data\GoglobalInterfaceFactory;
use Kowal\GoGlobal\Api\Data\GoglobalSearchResultsInterfaceFactory;
use Kowal\GoGlobal\Api\GoglobalRepositoryInterface;
use Kowal\GoGlobal\Model\ResourceModel\Goglobal as ResourceGoglobal;
use Kowal\GoGlobal\Model\ResourceModel\Goglobal\CollectionFactory as GoglobalCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class GoglobalRepository implements GoglobalRepositoryInterface
{

    /**
     * @var GoglobalCollectionFactory
     */
    protected $goglobalCollectionFactory;

    /**
     * @var ResourceGoglobal
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var GoglobalInterfaceFactory
     */
    protected $goglobalFactory;

    /**
     * @var Goglobal
     */
    protected $searchResultsFactory;


    /**
     * @param ResourceGoglobal $resource
     * @param GoglobalInterfaceFactory $goglobalFactory
     * @param GoglobalCollectionFactory $goglobalCollectionFactory
     * @param GoglobalSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceGoglobal $resource,
        GoglobalInterfaceFactory $goglobalFactory,
        GoglobalCollectionFactory $goglobalCollectionFactory,
        GoglobalSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->goglobalFactory = $goglobalFactory;
        $this->goglobalCollectionFactory = $goglobalCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(GoglobalInterface $goglobal)
    {
        try {
            $this->resource->save($goglobal);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the goglobal: %1',
                $exception->getMessage()
            ));
        }
        return $goglobal;
    }

    /**
     * @inheritDoc
     */
    public function get($goglobalId)
    {
        $goglobal = $this->goglobalFactory->create();
        $this->resource->load($goglobal, $goglobalId);
        if (!$goglobal->getId()) {
            throw new NoSuchEntityException(__('Goglobal with id "%1" does not exist.', $goglobalId));
        }
        return $goglobal;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->goglobalCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(GoglobalInterface $goglobal)
    {
        try {
            $goglobalModel = $this->goglobalFactory->create();
            $this->resource->load($goglobalModel, $goglobal->getGoglobalId());
            $this->resource->delete($goglobalModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Goglobal: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($goglobalId)
    {
        return $this->delete($this->get($goglobalId));
    }
}

