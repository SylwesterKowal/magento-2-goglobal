<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class GoglobalShipment extends AbstractDb
{

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('kowal_goglobal_goglobalshipment', 'goglobalshipment_id');
    }
}

