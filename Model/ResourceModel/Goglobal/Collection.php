<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Model\ResourceModel\Goglobal;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'goglobal_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\GoGlobal\Model\Goglobal::class,
            \Kowal\GoGlobal\Model\ResourceModel\Goglobal::class
        );
    }
}

