<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Api\Data;

interface GoglobalSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Goglobal list.
     * @return \Kowal\GoGlobal\Api\Data\GoglobalInterface[]
     */
    public function getItems();

    /**
     * Set kraj list.
     * @param \Kowal\GoGlobal\Api\Data\GoglobalInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

