<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Api\Data;

interface GoglobalInterface
{

    const SERWIS = 'serwis';
    const METODA = 'metoda';
    const KURIER = 'kurier';
    const TYP = 'typ';
    const WAGA = 'waga';
    const KRAJ = 'kraj';
    const GOGLOBAL_ID = 'goglobal_id';
    const TRACKING_URL_TEMPLATE = 'tracking_url_template';

    /**
     * Get goglobal_id
     * @return string|null
     */
    public function getGoglobalId();

    /**
     * Set goglobal_id
     * @param string $goglobalId
     * @return \Kowal\GoGlobal\Goglobal\Api\Data\GoglobalInterface
     */
    public function setGoglobalId($goglobalId);

    /**
     * Get kraj
     * @return string|null
     */
    public function getKraj();

    /**
     * Set kraj
     * @param string $kraj
     * @return \Kowal\GoGlobal\Goglobal\Api\Data\GoglobalInterface
     */
    public function setKraj($kraj);

    /**
     * Get kurier
     * @return string|null
     */
    public function getKurier();

    /**
     * Set kurier
     * @param string $kurier
     * @return \Kowal\GoGlobal\Goglobal\Api\Data\GoglobalInterface
     */
    public function setKurier($kurier);

    /**
     * Get serwis
     * @return string|null
     */
    public function getSerwis();

    /**
     * Set serwis
     * @param string $serwis
     * @return \Kowal\GoGlobal\Goglobal\Api\Data\GoglobalInterface
     */
    public function setSerwis($serwis);

    /**
     * Get typ
     * @return string|null
     */
    public function getTyp();

    /**
     * Set typ
     * @param string $typ
     * @return \Kowal\GoGlobal\Goglobal\Api\Data\GoglobalInterface
     */
    public function setTyp($typ);

    /**
     * Get waga
     * @return string|null
     */
    public function getWaga();

    /**
     * Set waga
     * @param string $waga
     * @return \Kowal\GoGlobal\Goglobal\Api\Data\GoglobalInterface
     */
    public function setWaga($waga);

    /**
     * Get metoda
     * @return string|null
     */
    public function getMetoda();

    /**
     * Set metoda
     * @param string $metoda
     * @return \Kowal\GoGlobal\Goglobal\Api\Data\GoglobalInterface
     */
    public function setMetoda($metoda);

    /**
     * Get tracking_url_template
     * @return string|null
     */
    public function getTrackingUrlTemplate();

    /**
     * Set tracking_url_template
     * @param string $trackingUrlTemplate
     * @return \Kowal\GoGlobal\Goglobal\Api\Data\GoglobalInterface
     */
    public function setTrackingUrlTemplate($trackingUrlTemplate);
}

