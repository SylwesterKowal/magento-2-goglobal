<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Api\Data;

interface GoglobalShipmentInterface
{

    const ORDER_ID = 'order_id';
    const TRACKING_NUMBER = 'tracking_number';
    const CREATED = 'created';
    const ID = 'id';
    const GOGLOBALSHIPMENT_ID = 'goglobalshipment_id';
    const STATUS = 'status';
    const MESSAGE_UID = 'message_uid';
    const BARCODE = 'barcode';
    const MESSAGE_CODE = 'message_code';
    const MESSAGE_CONTENT = 'message_content';
    const REFERENCE_ID = 'reference_id';
    const LABELFORMAT = 'label_format';
    const TRACKING_URL = 'tracking_url';
    const LABELDATA = 'label_data';
    const KURIER = 'kurier';
    const KURIER_SERVICE = 'kurier_service';
    const KURIER_TYPE = 'kurier_type';

    /**
     * Get goglobalshipment_id
     * @return string|null
     */
    public function getGoglobalshipmentId();

    /**
     * Set goglobalshipment_id
     * @param string $goglobalshipmentId
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setGoglobalshipmentId($goglobalshipmentId);

    /**
     * Get reference_id
     * @return string|null
     */
    public function getReferenceId();

    /**
     * Set reference_id
     * @param string $referenceId
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setReferenceId($referenceId);

    /**
     * Get created
     * @return string|null
     */
    public function getCreated();

    /**
     * Set created
     * @param string $created
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setCreated($created);

    /**
     * Get message_uid
     * @return string|null
     */
    public function getMessageUid();

    /**
     * Set message_uid
     * @param string $messageUid
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setMessageUid($messageUid);

    /**
     * Get message_code
     * @return string|null
     */
    public function getMessageCode();

    /**
     * Set message_code
     * @param string $messageCode
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setMessageCode($messageCode);

    /**
     * Get message_content
     * @return string|null
     */
    public function getMessageContent();

    /**
     * Set message_content
     * @param string $messageContent
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setMessageContent($messageContent);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setStatus($status);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setId($id);

    /**
     * Get barcode
     * @return string|null
     */
    public function getBarcode();

    /**
     * Set barcode
     * @param string $barcode
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setBarcode($barcode);

    /**
     * Get tracking_number
     * @return string|null
     */
    public function getTrackingNumber();

    /**
     * Set tracking_number
     * @param string $trackingNumber
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setTrackingNumber($trackingNumber);

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     * @param string $orderId
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setOrderId($orderId);

    /**
     * Get tracking_url
     * @return string|null
     */
    public function getTrackingUrl();

    /**
     * Set tracking_url
     * @param string $trackingUrl
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setTrackingUrl($trackingUrl);

    /**
     * Get label_data
     * @return string|null
     */
    public function getLabelData();

    /**
     * Set label_data
     * @param string $labelData
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setLabelData($labelData);

    /**
     * Get label_format
     * @return string|null
     */
    public function getLabelFormat();

    /**
     * Set label_format
     * @param string $labelFormat
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setLabelFormat($labelFormat);

    /**
     * Get kurier
     * @return string|null
     */
    public function getKurier();

    /**
     * Set kurier
     * @param string $kurier
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setKurier($kurier);

    /**
     * Get kurier_service
     * @return string|null
     */
    public function getKurierService();

    /**
     * Set kurier_service
     * @param string $kurierService
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setKurierService($kurierService);

    /**
     * Get kurier_type
     * @return string|null
     */
    public function getKurierType();

    /**
     * Set kurier_type
     * @param string $kurierType
     * @return \Kowal\GoGlobal\GoglobalShipment\Api\Data\GoglobalShipmentInterface
     */
    public function setKurierType($kurierType);
}

