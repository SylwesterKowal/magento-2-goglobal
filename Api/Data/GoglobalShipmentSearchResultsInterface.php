<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Api\Data;

interface GoglobalShipmentSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get GoglobalShipment list.
     * @return \Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface[]
     */
    public function getItems();

    /**
     * Set referenceID list.
     * @param \Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

