<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface GoglobalRepositoryInterface
{

    /**
     * Save Goglobal
     * @param \Kowal\GoGlobal\Api\Data\GoglobalInterface $goglobal
     * @return \Kowal\GoGlobal\Api\Data\GoglobalInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\GoGlobal\Api\Data\GoglobalInterface $goglobal
    );

    /**
     * Retrieve Goglobal
     * @param string $goglobalId
     * @return \Kowal\GoGlobal\Api\Data\GoglobalInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($goglobalId);

    /**
     * Retrieve Goglobal matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\GoGlobal\Api\Data\GoglobalSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Goglobal
     * @param \Kowal\GoGlobal\Api\Data\GoglobalInterface $goglobal
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\GoGlobal\Api\Data\GoglobalInterface $goglobal
    );

    /**
     * Delete Goglobal by ID
     * @param string $goglobalId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($goglobalId);
}

