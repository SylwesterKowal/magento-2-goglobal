<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface GoglobalShipmentRepositoryInterface
{

    /**
     * Save GoglobalShipment
     * @param \Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface $goglobalShipment
     * @return \Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface $goglobalShipment
    );

    /**
     * Retrieve GoglobalShipment
     * @param string $goglobalshipmentId
     * @return \Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($goglobalshipmentId);

    /**
     * Retrieve GoglobalShipment matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\GoGlobal\Api\Data\GoglobalShipmentSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete GoglobalShipment
     * @param \Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface $goglobalShipment
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\GoGlobal\Api\Data\GoglobalShipmentInterface $goglobalShipment
    );

    /**
     * Delete GoglobalShipment by ID
     * @param string $goglobalshipmentId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($goglobalshipmentId);
}

