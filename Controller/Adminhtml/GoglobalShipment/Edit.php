<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Controller\Adminhtml\GoglobalShipment;

class Edit extends \Kowal\GoGlobal\Controller\Adminhtml\GoglobalShipment
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('goglobalshipment_id');
        $model = $this->_objectManager->create(\Kowal\GoGlobal\Model\GoglobalShipment::class);
        
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Goglobalshipment no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('kowal_goglobal_goglobalshipment', $model);
        
        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Goglobalshipment') : __('New Goglobalshipment'),
            $id ? __('Edit Goglobalshipment') : __('New Goglobalshipment')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Goglobalshipments'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Goglobalshipment %1', $model->getId()) : __('New Goglobalshipment'));
        return $resultPage;
    }
}

