<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Controller\Adminhtml\GoglobalShipment;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;
    protected $order_id;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                   $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Magento\Sales\Api\OrderRepositoryInterface           $orderRepository,
        \Kowal\GoGlobal\Model\Service\Shipment\Create         $createShipment,
        \Kowal\GoGlobal\Model\Config\ConfigProvider           $configProvider,
        \Kowal\GoGlobal\Model\Service\Service                 $service,
        \Kowal\GoGlobal\Helper\Query                          $query
    )
    {
        $this->dataPersistor = $dataPersistor;
        $this->orderRepository = $orderRepository;
        $this->createShipment = $createShipment;
        $this->configProvider = $configProvider;
        $this->service = $service;
        $this->query = $query;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
//        $data_ = $this->getRequest()->getPostValue();


        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();


        try {
            $order_id = $this->getRequest()->getParam('order_id');

            $order = $this->orderRepository->get($order_id);

            $settings = [
                'hash' => $this->configProvider->getApiHash(),
                'mode' => $this->configProvider->getMode(),
                'file_type' => $this->configProvider->getFormatLabel(),
                'format_label' => $this->configProvider->getFileType(),
                'tariffCode' => $this->configProvider->getTariffCode()
            ];
            $this->createShipment->createBody($settings, $order);

            $respond = $this->service->call($this->configProvider->getApiBaseUrl() . 'create-shipment', $this->createShipment->requestBody);

            $data = [];
            if (isset($respond['status']) && $respond['status'] == 'Error') {
                $data['status'] = $respond['status'];
                $data['message_uid'] = (isset($respond['message']['uid'])) ? $respond['message']['uid'] : "";
                $data['message_code'] = (isset($respond['message']['code'])) ? $respond['message']['code'] : "";
                $data['message_content'] = ((isset($respond['message']['content'])) ? $respond['message']['content'] : "") . PHP_EOL . print_r(json_encode($this->createShipment->requestBody), true);
                $data['order_id'] = (string)$order->getId();
                $data['kurier'] = (string)$this->createShipment->deliverySettings->getKurier();
                $data['kurier_service'] = (string)$this->createShipment->deliverySettings->getSerwis();
                $data['kurier_type'] = (string)$this->createShipment->deliverySettings->getTyp();
            } else {
                $data['reference_id'] = (isset($respond['responses'][0]['referenceID'])) ? $respond['responses'][0]['referenceID'] : "";
                $data['status'] = (isset($respond['responses'][0]['status'])) ? (string)$respond['responses'][0]['status'] : "";
//            $data['id'] = (isset($respond['responses'][0]['id'])) ? (string)$respond['responses'][0]['id'] : "";
                $data['barcode'] = (isset($respond['responses'][0]['barcode'])) ? (string)$respond['responses'][0]['barcode'] : "";
                $data['tracking_number'] = (isset($respond['responses'][0]['tracking_number'])) ? (string)$respond['responses'][0]['tracking_number'] : "";
                $data['message_uid'] = (isset($respond['responses'][0]['message']['uid'])) ? (string)$respond['responses'][0]['message']['uid'] : "";
                $data['message_code'] = (isset($respond['responses'][0]['message']['code'])) ? (string)$respond['responses'][0]['message']['code'] : "";
                $data['message_content'] = (isset($respond['responses'][0]['message']['content'])) ? (string)$respond['responses'][0]['message']['content'] : "";
//            $data['created'] = date('Y-m-d');
                $data['order_id'] = (string)$order->getId();
                if ($url_tmeplate = (string)$this->createShipment->deliverySettings->getTrackingUrlTemplate()) {
                    $data['tracking_url'] = str_replace("{tracking_number}", $data['tracking_number'], $url_tmeplate);
                }
                if ($data['reference_id']) {
                    $this->createShipment->createBodyLabel($settings, $data['reference_id']);
                    if ($respondLabel = $this->service->call($this->configProvider->getApiBaseUrl() . 'get-label', $this->createShipment->requestBodyLabel)) {

                        $data['label_data'] = (isset($respondLabel['responses'][0]['labelData'])) ? (string)$respondLabel['responses'][0]['labelData'] : "";
                        $data['label_format'] = (isset($respondLabel['responses'][0]['labelFormat'])) ? (string)$respondLabel['responses'][0]['labelFormat'] : "";

                    }
                }

                $data['kurier'] = (string)$this->createShipment->deliverySettings->getKurier();
                $data['kurier_service'] = (string)$this->createShipment->deliverySettings->getSerwis();
                $data['kurier_type'] = (string)$this->createShipment->deliverySettings->getTyp();
            }

            if (isset($data['label_data']) && !empty($data['label_data'])) {
                $this->query->updateOrderLabel($order->getEntityId(), base64_decode((string)$data['label_data']), "OK");
            }


            $model = $this->_objectManager->create(\Kowal\GoGlobal\Model\GoglobalShipment::class)->load(null);
            $model->setData($data);

            $model->save();



            $this->messageManager->addSuccessMessage(__('You saved the Goglobalshipment.'));

            $this->dataPersistor->clear('kowal_goglobal_goglobalshipment');

            return $resultRedirect->setPath(
                'sales/order/view',
                ['order_id' => $this->getRequest()->getParam('order_id')]
            );

        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Goglobalshipment.'));
        }

        $this->dataPersistor->set('kowal_goglobal_goglobalshipment', $data);
        return $resultRedirect->setPath(
            'sales/order/view',
            ['order_id' => $this->getRequest()->getParam('order_id')]
        );

    }

    public function setOrderData($order_id)
    {
        $this->order_id = $order_id;
        return $this;
    }
}

