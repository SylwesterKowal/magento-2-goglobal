# Mage2 Module Kowal GoGlobal

    ``kowal/module-goglobal``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Integracja Magento 2 GoGlobal

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_GoGlobal`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-goglobal`
 - enable the module by running `php bin/magento module:enable Kowal_GoGlobal`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (goglobal/options/enable)

 - mode (goglobal/api/mode)

 - api_url_pro (goglobal/api/api_url_pro)

 - api_url_dev (goglobal/api/api_url_dev)

 - api_url_tracking (goglobal/api/api_url_tracking)

 - api_typ_pliku (goglobal/api/api_typ_pliku)

 - api_format (goglobal/api/api_format)

 - api_wielkosc_paczki (goglobal/api/api_wielkosc_paczki)

 - api_username (goglobal/autoryzacja/api_username)

 - api_password (goglobal/autoryzacja/api_password)

 - api_hash (goglobal/autoryzacja/api_hash)

 - kurierzy (goglobal/source/kurierzy)

 - kraje (goglobal/source/kraje)

 - magento_shippings (goglobal/source/magento_shippings)


## Specifications




## Attributes



