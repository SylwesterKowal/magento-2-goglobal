<?php
declare(strict_types=1);

namespace Kowal\GoGlobal\Block\Adminhtml\Order\View;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Sales\Block\Adminhtml\Order\AbstractOrder;
use Magento\Sales\Helper\Admin;
use Magento\Shipping\Helper\Data as ShippingHelper;
use Magento\Tax\Helper\Data as TaxHelper;
use Kowal\GoGlobal\Model\Config\Source\MagentoShippings;
use Kowal\GoGlobal\Model\GoglobalShipmentRepository;
use Kowal\GoGlobal\Model\ResourceModel\GoglobalShipment\CollectionFactory as CollectionFactoryGoglobalShipment;


class Goglobal extends AbstractOrder
{
    /**
     * @var ShippingMethods
     */
    protected $shippingMethods;

    /**
     * @var \Kowal\GoGlobal\Model\GoglobalShipmentRepository
     */
    protected $goglobalShipmentRepository;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param Admin $adminHelper
     * @param MagentoShippings $shippingMethods
     * @param GoglobalShipmentRepository $goglobalShipmentRepository
     * @param array $data
     * @param ShippingHelper|null $shippingHelper
     * @param TaxHelper|null $taxHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Admin $adminHelper,
        MagentoShippings $shippingMethods,
        GoglobalShipmentRepository $goglobalShipmentRepository,
        CollectionFactoryGoglobalShipment $collectionFactoryGoglobalShipment,
        array $data = [],
        ?ShippingHelper $shippingHelper = null,
        ?TaxHelper $taxHelper = null
    ) {
        $this->shippingMethods = $shippingMethods;
        $this->goglobalShipmentRepository = $goglobalShipmentRepository;
        $this->collectionFactoryGoglobalShipment = $collectionFactoryGoglobalShipment;

        parent::__construct($context, $registry, $adminHelper, $data);
    }

    /**
     * @return array
     */
    public function getGoGlobalShippingMethods() : array
    {
        return $this->shippingMethods->toOptionArray(true);
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSelectedMethod() : string
    {
        if($ship = $this->getOrder()->getShippingMethod()){
            return $ship;
        }else{
            return "";
        }

    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getGoglobalShipments()
    {
        $order_id = $this->getOrder()->getId();
        $collectionGoglobalShipment = $this->collectionFactoryGoglobalShipment->create();
        $collectionGoglobalShipment->addFieldToFilter('order_id', array('eq' => $order_id));
        return $collectionGoglobalShipment->addFieldToSelect('*');
    }



    /**
     * @param $shipment
     * @return array
     */
    public function getShippingDetails($shipment)
    {
        $details = [];
        $details[ShipmentInterface::STATUS] = $this->statusConfig->getStatusLabel($shipment->getStatus());
        if (strpos($shipment->getService(), 'inpost_locker') !== false) {
            $details[ShipmentInterface::SHIPMENT_ATTRIBUTES] =
                $this->sizeConfig->getSizeLabel($shipment->getShipmentsAttributes());
            $details[ShipmentInterface::TARGET_POINT] =  __("Point: ") . $shipment->getTargetPoint();
        }
        return $details;
    }

    /**
     * @param $shipment
     * @return string
     */
    public function getLabelUrl($shipment)
    {
        return base64_decode((string)$shipment->getLabelData());
    }

    /**
     * @param $shipment
     * @return boolean
     */
    public function isReturnPossible($shipment)
    {

            return false;
    }

    /**
     * @param $shipment
     * @return string
     */
    public function getReturnUrl($shipment)
    {

        return "";
    }

    public function isUrlBlank($shipment)
    {
        if ($shipment->getService() != 'inpost_locker_standard') {
            return false;
        }

        return true;
    }

    /**
     * @param $inpostShipmentId
     * @return null
     */
    public function getGoglobalShippment($inpostShipmentId)
    {
        $shipment = null;
        try {
            $shipment =  $this->goglobalShipmentRepository->getBygGoglobalshipmentId($inpostShipmentId);
        } catch (\Exception $e) {
        }
        return $shipment;
    }

    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getInPostToken()
    {
        return $this->configProvider->getShippingConfigData('geowidget_token');
    }
}
