<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoGlobal\Console\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Kowal\GoGlobal\Helper\Query;
use Kowal\WFirma\lib\Config;
use Kowal\GoGlobal\Model\Config\ConfigProvider;
use Kowal\GoGlobal\Model\Service\Shipment\Create as CreateShippment;
use Kowal\GoGlobal\Model\Service\Service;
use Magento\Framework\ObjectManagerInterface;

class GenerujLabel extends Command
{

    const ORDER_ID = "order_id";
    const NAME_OPTION = "option";

    protected $goGlobal = ['matrixrate', 'flatrate'];

    public function __construct(

        OrderCollectionFactory $collectionFactory,
        Query                  $query,
        Config                 $config,
        ConfigProvider         $configProvider,
        CreateShippment        $createShipment,
        Service                $service,
        ObjectManagerInterface $objectManager
    )
    {

        $this->collectionFactory = $collectionFactory;
        $this->query = $query;
        $this->config = $config;
        $this->configProvider = $configProvider;
        $this->createShipment = $createShipment;
        $this->service = $service;
        $this->objectManager = $objectManager;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        try {
            $option = $input->getOption(self::NAME_OPTION);


            if ($order_id = $input->getArgument(self::ORDER_ID)) {

                $collection = $this->getOrdersCollection(['entity_id' => ['eq' => $order_id],
                    'status' => ['in' => [
                        $this->config->getOrderStatusDoZleceniaProd(),
                        $this->config->getOrderStatusDoWysylki(),
                        $this->config->getOrderStatusDoProdukcji(),
                        $this->config->getOrderStatusDoPakowania()]]
                ]);
                foreach ($collection as $order) {
                    $result = $this->saveLabel($order);
                    $output->writeln("TEST ORDER: " . $order->getEntityId()) . ' ' . $result;
                }

            } else {
                $collection = $this->getOrdersCollection(['shipment_label_status' => ['null' => true, 'eq' => ''],
                    'status' => ['in' => [
                        $this->config->getOrderStatusDoZleceniaProd(),
                        $this->config->getOrderStatusDoWysylki(),
                        $this->config->getOrderStatusDoProdukcji(),
                        $this->config->getOrderStatusDoPakowania()]]
                ]);
                $label = "";
                foreach ($collection as $order) {
                    $this->saveLabel($order);
                    $output->writeln(date('Y-m-d H:i:s')." ORDER: " . $order->getEntityId(). ' '.$order->getShippingMethod());
                    sleep(5);
                }
            }

        } catch (\Exception $e) {
            $output->writeln("ERROR " . $e->getMessage());
        }
        return 1;


    }

    public function saveLabel($order)
    {
        $shiping = explode("_",$order->getShippingMethod())[0];
        echo $country = $order->getShippingAddress()->getCountryId();
        if (in_array($shiping, $this->goGlobal) && empty($order->getPaczkomat()) && $country != 'PL' ) {
            if ($label = $this->saveGoGlobal($order)) {
                $this->query->updateOrderLabel($order->getEntityId(), $label, "OK");
            } else {
                $this->query->updateOrderLabel($order->getEntityId(), "", "ERR");
            }
        } else {
            echo " POMIN";
        }
    }

    private function saveGoGlobal($order)
    {

        try {

            $settings = [
                'hash' => $this->configProvider->getApiHash(),
                'mode' => $this->configProvider->getMode(),
                'file_type' => $this->configProvider->getFormatLabel(),
                'format_label' => $this->configProvider->getFileType(),
                'tariffCode' => $this->configProvider->getTariffCode()
            ];
            $this->createShipment->createBody($settings, $order);

            $respond = $this->service->call($this->configProvider->getApiBaseUrl() . 'create-shipment', $this->createShipment->requestBody);

            $data = [];
            if (isset($respond['status']) && $respond['status'] == 'Error') {
                $data['status'] = $respond['status'];
                $data['message_uid'] = (isset($respond['message']['uid'])) ? $respond['message']['uid'] : "";
                $data['message_code'] = (isset($respond['message']['code'])) ? $respond['message']['code'] : "";
                $data['message_content'] = ((isset($respond['message']['content'])) ? $respond['message']['content'] : "") . PHP_EOL . print_r(json_encode($this->createShipment->requestBody), true).PHP_EOL.print_r($respond,true);
                $data['order_id'] = (string)$order->getId();
                $data['kurier'] = (string)$this->createShipment->deliverySettings->getKurier();
                $data['kurier_service'] = (string)$this->createShipment->deliverySettings->getSerwis();
                $data['kurier_type'] = (string)$this->createShipment->deliverySettings->getTyp();
            } else {
                $data['reference_id'] = (isset($respond['responses'][0]['referenceID'])) ? $respond['responses'][0]['referenceID'] : "";
                $data['status'] = (isset($respond['responses'][0]['status'])) ? (string)$respond['responses'][0]['status'] : "";
//            $data['id'] = (isset($respond['responses'][0]['id'])) ? (string)$respond['responses'][0]['id'] : "";
                $data['barcode'] = (isset($respond['responses'][0]['barcode'])) ? (string)$respond['responses'][0]['barcode'] : "";
                $data['tracking_number'] = (isset($respond['responses'][0]['tracking_number'])) ? (string)$respond['responses'][0]['tracking_number'] : "";
                $data['message_uid'] = (isset($respond['responses'][0]['message']['uid'])) ? (string)$respond['responses'][0]['message']['uid'] : "";
                $data['message_code'] = (isset($respond['responses'][0]['message']['code'])) ? (string)$respond['responses'][0]['message']['code'] : "";
                $data['message_content'] = (isset($respond['responses'][0]['message']['content'])) ? (string)$respond['responses'][0]['message']['content'] : "";
//            $data['created'] = date('Y-m-d');
                $data['order_id'] = (string)$order->getId();
                if ($url_tmeplate = (string)$this->createShipment->deliverySettings->getTrackingUrlTemplate()) {
                    $data['tracking_url'] = str_replace("{tracking_number}", $data['tracking_number'], $url_tmeplate);
                }
                if ($data['reference_id']) {
                    $this->createShipment->createBodyLabel($settings, $data['reference_id']);
                    $respondLabel = $this->service->call($this->configProvider->getApiBaseUrl() . 'get-label', $this->createShipment->requestBodyLabel);

                    $data['label_data'] = (isset($respondLabel['responses'][0]['labelData'])) ? (string)$respondLabel['responses'][0]['labelData'] : "";
                    $data['label_format'] = (isset($respondLabel['responses'][0]['labelFormat'])) ? (string)$respondLabel['responses'][0]['labelFormat'] : "";
                }

                $data['kurier'] = (string)$this->createShipment->deliverySettings->getKurier();
                $data['kurier_service'] = (string)$this->createShipment->deliverySettings->getSerwis();
                $data['kurier_type'] = (string)$this->createShipment->deliverySettings->getTyp();
            }
            #TODO zmienić status zamówienia po wygenerowaniu etykiety


            $model = $this->objectManager->create(\Kowal\GoGlobal\Model\GoglobalShipment::class)->load(null);
            $model->setData($data);

            $model->save();

            if ((isset($respondLabel['responses'][0]['labelData']))) {
                return base64_decode((string)$model->getLabelData());
            } else {
                echo print_r($data, true);
                return false;
            }


        } catch (LocalizedException $e) {
            echo $e->getMessage();
            return false;
        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getOrdersCollection(array $filters = [])
    {

        $collection = $this->collectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_goglobal:generujlabel");
        $this->setDescription("GoGlobal");
        $this->setDefinition([
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Order Entity ID"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}
